PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man
DOCBUILDDIR = doc/build
MANBUILDDIR = $(DOCBUILDDIR)/man

install: man
	pip3 install --install-option="--prefix=$(PREFIX)" .

uninstall:
	rm -f $(MANDIR)/man1/i3bf*
	rm -f $(PREFIX)/bin/i3bf*
	pip3 uninstall --yes i3barfodder

develop:
	pip3 install --install-option="--prefix=$(PREFIX)" --editable .

test:
	nosetests3 --stop

man:
	devtools/make_manpages.sh $(MANBUILDDIR)/man1
	install -d $(MANDIR)/man1
	install -m 644 $(MANBUILDDIR)/man1/* $(MANDIR)/man1

clean:
	rm -rf build dist
	rm -rf $(DOCBUILDDIR)
	find . -name "*.pyc" -delete
	find . -name "__pycache__" -delete
	rm -f devtools/screencast/screencast.raw.mkv
