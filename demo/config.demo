# This config and the workers it uses are meant to test and demonstrate most
# of i3barfodder's core features.  They are NOT meant to be used as a
# reference for real-world usage and best practices.
#
# Use this file as a tutorial to learn the basic principles of i3barfodder.
#
# See workers/i3bf-* and the example config file for more useful examples that
# use i3bfutils, which makes everything much easier.

# Log debugging messages to this file.
# Whatever workers print to STDERR will end up in there if i3bf is called with
# -d/--debug.
# logfile = ~/i3bardemo.log

# Display the number of updates pushed to i3bar over the last 10 seconds.
show_updates = True

# By default, i3barfodder waits 0.1 seconds after a worker pushes a new line
# and then checks if any other workers have pushed something.  This is an
# attempt to pool updates together for better efficiency.  With the default
# 100ms delay, there is an upper limit of 10 updates per second.  You can
# disable or adjust this by setting 'delay', but the default shouldn't be
# noticable.
#delay = 0

# You can set arbitrary environment variables for each section or globally for
# all sections.
# PATH is a bit special as it is appended to the existing PATH, and using ~
# works as expected.
PATH = /some/path:~/bin
LANG = en_GB.UTF-8

# Some default settings for all sections.
color = #7f4
separator_block_width = 25

# Display time with pango markup
[time]
command = time_display
FORMAT = %H:%M<small>:%S</small>
markup = pango
color = #9be
# Customize separator between this and next section
separator_block_width = 10
separator = false

# In this example, the current date is cached every second, but it doesn't
# trigger an i3bar update.  The latest output will be displayed when another
# section triggers an update (e.g. [time]).
[date]
trigger_updates = false
command = while true; do date '+%Y-%m-%d'; sleep 1; done
shell = true
color = #68d

# It's possible to not set a worker and define 'full_text' instead.  (Although
# I don't see how this would be useful.)
[static text]
full_text = This text never changes
color = #f93

# If a worker exits without error (exitcode 0), its last line is displayed
# forever.
# Setting shell to true will run 'command' via '/bin/sh -c'.
# Valid boolean values are true/false, on/off, yes/no and 1/0.
[kernel version]
shell = True
command = echo "Linux $(uname -r)"

# Workers can print JSON objects to overload their config file settings.  This
# works for fields from the i3bar protocol aswell as i3barfodder's extensions.
# This example demonstrates this by changing the worker command, which kills
# the command and calls it again.
# It also shows how to display a vertical bar.
[bouncing counter]
command = bouncycount
color = #c9f

# vbars can use different scales ('linear', 'sqrt' and 'log'); this
# demonstrates each of them as a graph made of multiple vertical bars.
[scales]
command = vbars
color = #F0E68C

# You can specify multiple colors and then mix them by using a number between
# min/max limits.
# This also demonstrates that a worker can display multiple blocks by printing
# an array of JSON objects.
[rainbow]
command = rainbow
WIDTH = 30
color = #f00-#0f0-#00f

# Another demonstration for multiple blocks, but with blocks being added and
# removed.
[scrolling text]
command = scrollingtext
color = #f0f-#ff0-#0ff
WIDTH = 20
