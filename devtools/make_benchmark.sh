#!/bin/sh

version="${1:-dev}"

if [ "$version" = 'dev' ]; then
    cmd='devtools/i3bf-dev'
else
    cmd='i3bf'
fi

date '+%Y-%m-%d %H:%M:%S'
$cmd --version
git log -1 --oneline
/usr/bin/time /usr/bin/timeout --preserve-status 60 $cmd -c demo/config.demo -p demo 2>&1 1>/dev/null
