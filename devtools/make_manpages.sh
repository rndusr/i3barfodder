#!/bin/bash
set -o errexit
set -o nounset

devtools_dir="$(realpath "$(dirname "$0")")"
source_dir="$devtools_dir/../doc/man"

if [ -z "$*" ]; then
    echo "Missing target directory" >&2
    exit 1
else
    mkdir -p "$1"
    target_dir="$(realpath "$1")"
    echo "Writing man pages to $target_dir" >&2
fi


function make_manpage {
    source_file="$1"
    title="$(basename "${source_file%.rst}")"
    target_file="$target_dir/$(basename "${title}.1")"
    if [ ! -e "$target_file" ]; then
        echo "Creating $target_file"

        pandoc --standalone -V section=1 -V title="$title" \
               --from=rst --to=man -o "$target_file" \
               <("$devtools_dir"/include_parser "$source_file")
    else
        echo "Skipping existing $target_file"
    fi
}


for f in "$source_dir/"i3bf*.rst; do
    make_manpage "$f"
done
