#!/bin/bash
set -o nounset

pause_and_clear() {
    sleep ${1:-3}
    clear
}

clear_line() {
    echo -en "\e[1000D\e[K"
}

fake_prompt() {
    echo -en '\e[1;30m$\e[0m '
}

fake_type() {
    text="$1"
    for (( i=0; i<${#text}; i++ )); do
        echo -n "${text:$i:1}"
        sleep 0.$(( RANDOM % 2 ))
    done
    echo
}

run() {
    cmd="$1"
    clear_line
    fake_prompt
    fake_type "$cmd"
    sh -c "$cmd"
    fake_prompt
}

say() {
    clear_line
    fake_prompt
    echo -en '\e[0;33m'
    fake_type "# $1"
    echo -en '\e[0m'
    fake_prompt
    sleep 1
}

clear
fake_prompt
sleep 1

say 'Network traffic'
run 'timeout 3 wget --quiet http://releases.ubuntu.com/14.04.3/ubuntu-14.04.3-desktop-amd64.iso'
rm ./ubuntu*.iso

pause_and_clear

say 'CPU load on 1 core'
run 'timeout 3 parallel burnK7 -- _'
sleep 1
say '... and on 4 cores'
run 'timeout 3 parallel burnK7 -- _ _ _ _'

pause_and_clear

# Create file system image to mount as a fake thumb drive
device='/tmp/fakeusb.img'
name='USB'
mountpoint="/media/$USER/$name"
dd if=/dev/zero of="$device" bs=1MB count=10 2>/dev/null
/sbin/mkfs.vfat -n "$name" "$device" >/dev/null 2>&1
udisksctl loop-setup --file "$device" >/dev/null

say 'New mountpoints are added dynamically'
run 'udisksctl mount --block-device /dev/loop0'
sleep 4

say 'An "LED" indicates IO activity'
run "dd if=/dev/zero of=$mountpoint/lotsazeros bs=1MB count=5"
sleep 4
say 'The "free" color changes as the file system is getting full'
run "dd if=/dev/zero of=$mountpoint/evenmorezeros bs=1MB count=3"
sleep 4
run "dd if=/dev/zero of=$mountpoint/enoughzeros bs=1kB count=1900"
sleep 4

say 'Unmounting removes it'
run 'udisksctl unmount --block-device /dev/loop0'
udisksctl loop-delete --block-device /dev/loop0
rm "$device"

pause_and_clear

say 'Email notification'
run 'echo -e "Foo\nBlah." | mail $USER'
sleep 2
run 'echo -e "Bar\nBlaaah!" | mail $USER'

pause_and_clear

say "That's all, folks!"
sleep 3
