#!/bin/bash
set -o nounset

screencast_size='878x195'
screencast_offset='1042,0'
terminal_geometry='108x11-10+25'

title="screencast"
filename="../../media/${title}.webm"
filename_raw="${title}.raw.mkv"

cd "$(dirname "$0")"

[ "$*" = "-f" ] && rm -f "$filename_raw"

if [ ! -e "$filename_raw" ]; then
    # Start recording
    ffmpeg -loglevel 16 -f x11grab -framerate 8 \
           -video_size "${screencast_size}"  \
           -i ":0.0+${screencast_offset}" \
           -vcodec huffyuv -y "$filename_raw" &
    ffmpeg_pid=$!
    echo "ffmpeg is running with PID $ffmpeg_pid"

    # Run activity generator in terminal
    (urxvt -title floating -geometry ${terminal_geometry} -e sh -c './generate_activity.sh') &
    generate_activity_pid=$!
    echo "generate_activity.sh is running with PID $generate_activity_pid"
    echo -n 'Waiting for generate_activity.sh to finish ...'
    while [ -e "/proc/$generate_activity_pid" ]; do
        sleep 0.5
    done
    echo ' finished.'

    kill $ffmpeg_pid
    wait $ffmpeg_pid
fi

# Cut off last second
video_length=$(mediainfo --Inform="Video;%Duration%" "$filename_raw")
video_endpos=$(( ($video_length - 1000) / 1000 ))

encode() {
    q=18
    ffmpeg -i "$filename_raw" -to "$video_endpos" -y \
           -codec:v vp8 -cpu-used 0 -threads 4 \
           -crf $q -qmin $q -qmax $q -f webm \
           "$@"
}

encode -pass 1 /dev/null
encode -pass 2 "$filename"
rm -f ffmpeg2pass*.log
