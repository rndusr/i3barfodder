Example configuration file
==========================

.. literalinclude:: config.example
   :language: ini
   :caption: ``$XDG_CONFIG_HOME/i3barfodder/config``
