Configuration happens exclusively by setting environment variables.  This
typically takes place in the i3barfodder configuration file.

For more information, see the sections **CONFIGURATION FILE** and **WORKER
ENVIRONMENT VARIABLES** in i3bf(1).
