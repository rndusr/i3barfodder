A layout string defines a template in which current information is inserted
before pushing it to i3barfodder and eventually i3bar.  Each piece of
information is represented by a placeholder.  Placeholders are short,
descriptive strings.  Each worker defines their own placeholders.

If a placeholder starts with an underscore ("_"), a machine-readable format
(i.e. a number) is inserted.  Use that to change the height of vertical bars
or dynamically change the color.  If there is no underscore, a human-readable
format is inserted.

Special characters can be escaped with a backslash ("\\").

A layout string defines at least one block, but can define multiple blocks by
enclosing text in square brackets ("[", "]").  In addition to "full_text", it
is also possible to define some other variables:

**color**
    Anything between a pipe ("|") and the closing square bracket ("]") of a
    block defines a dynamic color string which is documented in the **I3BAR
    PROTOCOL VARIABLES** section in i3bf(1).  The value of the dynamic color
    is typically a placeholder that is resolved to an actual number during
    each update.

    In the following example, "Foo" is displayed in a color between green and
    red according to a percentage::

        [Foo|#0f0-#f00:min=0:max=100:value={_percent}]


**separator_block_width**, **separator**
    A number after the closing bracket of the block specifies the separator width
    to the next block.  If it is followed by a pipe ("|"), a separator line is
    drawn by i3bar.

    .. note::
       "separator" and "separator_block_width" of the final/rightmost block
       are always reset to the values configured in the user config or
       whatever i3bar uses as defaults.


CONDITIONALS
^^^^^^^^^^^^

Conditionals are used to display text only if a condition is met.  They look
like this::

    (SOMETHING?[=]CONDITION)

SOMETHING is any text with placeholders, function calls, etc as would be
accepted outside of a conditional.

CONDITION compares two numeric values.  Valid operators are "<", ">", "\<=",
">=", "=" and "!=".  Values can be fixed numbers or placeholders that expand
to machine-readable numbers (i.e. those with a leading underscore).  If
CONDITION is true, the conditional evaluates to SOMETHING, otherwise to an
empty string.

However, if the question mark between SOMETHING and CONDITION is followed by
an equal sign ("=") and CONDITION is false, the conditional evaluates to a
string of spaces with the same length as SOMETHING.  This is useful to always
get the same width whether CONDITION is true or not.
