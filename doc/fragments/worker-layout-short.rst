A template that specifies which information is displayed and how it is
formatted.  Information is inserted by replacing placeholders.  See the
**LAYOUT** section for more information.
