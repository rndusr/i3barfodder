NAME
====

i3bf-mail - Number of unread/read mails


DESCRIPTION
===========

A worker for the i3bar status line generator **i3barfodder** that displays
the number of emails in an email account.

Only maildir directories are supported at the moment.


CONFIGURATION
=============

.. include:: ../fragments/worker-configuration.rst


ENVIRONMENT VARIABLES
---------------------

**ACCOUNT** = *account*  (must be specified)
    Where to look for new mail.  The format is PROTOCOL:URL.  What URL means
    depends on PROTOCOL:

    +----------+-----------------------------------------------+
    | PROTOCOL | Meaning of URL                                |
    +==========+===============================================+
    | maildir  | Path to maildir directory                     |
    |          | (must contain subdirectories "new" and "cur") |
    +----------+-----------------------------------------------+

**LAYOUT** = *string*  (default: see below)
    .. include:: ../fragments/worker-layout-short.rst

    The default layout is::

        ({unseen} new mail(s?{_unseen}!=1)?{_unseen}>0)

    This defines a single block that displays "{unseen} new mail", but only if
    there are unseen mails in **ACCOUNT**.  If there are more than one unseen
    mails, it's "mails" instead of "mail".


PLACEHOLDERS
------------

**{unseen}**
    The number of unseen mails in **ACCOUNT**.

**{seen}**
    The number of seen mails in **ACCOUNT**.


LAYOUT
------

.. include:: ../fragments/worker-layout-long.rst


SEE ALSO
========

i3bf(1)
