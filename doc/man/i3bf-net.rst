NAME
====

i3bf-net - Network interface throughput


DESCRIPTION
===========

A worker for the i3bar status line generator **i3barfodder** that displays
the transfer rate of a network interface.


CONFIGURATION
=============

.. include:: ../fragments/worker-configuration.rst


ENVIRONMENT VARIABLES
---------------------

**NIC** = *string*  (default: auto-detect)
    Networking device to monitor, e.g. "eth0".

**INTERVAL** = *number*  (default: 1)
    Seconds between updates.

**SAMPLES** = *integer*  (default: 3)
    Number of previously measured rates to compute an average.  Increasing
    this value makes the display less jumpy, decreasing makes it more
    responsive.

**UNIT** = *unit*  (default: bit)
    Display rates in "bit" or "byte".

**BINARY** = *boolean*  (default: False)
    Which kind of binary prefix to use for rates.  False for multiples of 1000
    (k, M, G, ...), true for multiples of 1024 (Ki, Mi, Gi, ...).

**LAYOUT** = *string*  (default: see below)
    .. include:: ../fragments/worker-layout-short.rst

    The default layout is::

        [
            <small>({rate_up:>8s}?={_rate_up}>DISPLAY_MIN_UP) </small>
            vbar({_rate_up}, max=BANDWIDTH_UP, scale=SCALE)
            |{_rate_up}:max=BANDWIDTH_UP:scale=SCALE:COLORS_UP
        ]  [
            <small>({rate_down:>8s}?={_rate_down}>DISPLAY_MIN_DOWN) </small>
            vbar({_rate_down}, max=BANDWIDTH_DOWN, scale=SCALE)
            |{_rate_down}:max=BANDWIDTH_DOWN:scale=SCALE:COLORS_DOWN
        ]

    This defines two blocks, one for upload and one for download, with two
    spaces between them.  Each block displays the throughput rate if it is
    larger than **DISPLAY_MIN_UP/DOWN** followed by a vertical bar.  Both
    blocks change color based on how much of **BANDWIDTH_UP/DOWN** is
    currently used.


DEFAULT LAYOUT VARIABLES
^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../fragments/worker-layout-default-variables.rst

**BANDWIDTH_UP** = *number*  (default: 56k)
    Maximum upload bandwidth for **NIC**.

**DISPLAY_MIN_UP** = *number*  (default: 5%)
    Minimum upload rate to show it as a number beside the bar.

**COLORS_UP** = *colorlist*  (default: #847-#F7D)
    List of colors that is used to dynamically change color depending on the
    current upload rate.

**BANDWIDTH_DOWN** = *number*  (default: 56k)
    Maximum download bandwidth for **NIC**.

**DISPLAY_MIN_DOWN** = *number*  (default: 5%)
    Minimum download rate to show it as a number beside the bar.

**COLORS_DOWN** = *colorlist*  (default: #487-#7FD)
    List of colors that is used to dynamically change color depending on the
    current download rate.

**SCALE** = *scale*  (default: linear)
    How much emphasis to put on smaller values.  Must be "linear", "sqrt" or
    "log".


PLACEHOLDERS
------------

**{nic}**
    Name of the monitored device (same as **NIC**).

**{rate_up}**
    Current upload rate on **NIC**.

**{rate_down}**
    Current download rate on **NIC**.


LAYOUT
------

.. include:: ../fragments/worker-layout-long.rst


SEE ALSO
========

i3bf(1)
