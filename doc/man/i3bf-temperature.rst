NAME
====

i3bf-temperature - System temperature readout via /sys/


DESCRIPTION
===========

A worker for the i3bar status line generator **i3barfodder** that displays
hardware temperature sensor data.


CONFIGURATION
=============

.. include:: ../fragments/worker-configuration.rst


ENVIRONMENT VARIABLES
---------------------

**SENSOR_PATH** = *path*  (must be set)
    Path to a readable file that provides a temperature in degrees or
    millidegrees.

    Example::

        $ cat /sys/class/hwmon/hwmon1/device/temp1_input
        34000

**INTERVAL** = *number*  (default: 5)
    Seconds between updates.

**LAYOUT** = *string*  (default: see below)
    .. include:: ../fragments/worker-layout-short.rst

    The default layout is::

        [
          <small>({temp}?={_temp}>=DISPLAY_MIN) </small>
          vbar({_temp}, min=TEMP_MIN, max=TEMP_MAX, scale=SCALE)
          |{_temp}:min=TEMP_MIN:max=TEMP_MAX:scale=SCALE:COLORS
        ]

    This defines a single block that displays the temperature in a
    human-readable format (e.g. "43°C") if it is larger or equal to
    **DISPLAY_MIN**.  It also displays a vertical bar that changes height
    based on the temperature range defined by **TEMP_RANGE**.  Both displays
    change color as the temperature rises.


DEFAULT LAYOUT VARIABLES
^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../fragments/worker-layout-default-variables.rst

**TEMP_RANGE** = *number*  (default: 40-60)
    Defines **TEMP_MIN** and **TEMP_MAX**.

**DISPLAY_MIN** = *number*  (default: 0%)
    The minimum temperature to display it as a number beside the vertical bar.
    If set to a percentage, it is relative to **TEMP_MIN**.  For example, with
    a temperature range of 40-60 and **DISPLAY_MIN** set to 50%, the
    temperature would be displayed as a number at 50°C and higher.

**COLORS** = *colorlist*  (default: #09B-#C80-#F20)
    The list of colors that are used to dynamically change display color
    depending on the current temperature.

**SCALE** = *scale*  (default: linear)
    How much emphasis to put on smaller values.  Must be "linear", "sqrt" or
    "log".


PLACEHOLDERS
------------

**{temp}**
    The current temperature as provided by **SENSOR_PATH**.


LAYOUT
------

.. include:: ../fragments/worker-layout-long.rst


SEE ALSO
========

i3bf(1)
