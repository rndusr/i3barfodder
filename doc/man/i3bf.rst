NAME
====

i3bf - a status line generator for i3bar


SYNOPSIS
========

**i3bf** [*OPTIONS*]


DESCRIPTION
===========

.. include:: ../fragments/i3bf-description.txt


USAGE
=====

To use i3barfodder, set *status_command* in your *~/.config/i3/config* to
`i3bf` followed any of the command line options.


OPTIONS
=======

**-c** *FILE*, **--config** *FILE*
    Path to the configuration file.

**-d**, **--debug**
    Log debugging information to STDERR a file.  Besides debugging messages
    from i3bf, everything workers print to STDERR is also logged instead of
    ignored.

**-l** *FILE*, **--logfile** *FILE*
    Log debugging information to *FILE* instead of STDERR.  This overrides the
    **logfile** configuration variable.

**-p** *PATH*, **--path** *PATH*
    Append *PATH* to the PATH environment variable.  Use this if your workers
    are tucked away outside of any of your PATHs.

    .. note::
       It is also possible to expand PATH in the configuration file by setting
       it globally.

**-V**, **--version**
    Display version number and exit.

**-h**, **--help**
    Display help text and exit.


CONFIGURATION FILE
==================

The default configuration file path is $XDG_CONFIG_HOME/i3barfodder/config.
If XDG_CONFIG_HOME is not set, it defaults to ~/.config.

Configuration is done in a simple INI-style-ish format with global variables
and section-local variables.  Each section must have a unique name and usually
specifies the variable **command**, but that is not required.  In fact, there
are no required variables; an empty section is perfectly valid (albeit not
very useful).

Variables can be set globally or locally.

Lines that have "#" as the first non-space character are ignored.

Besides setting variables with "=", it is also possible to append to existing
values with "+=".

Leading and trailing spaces in values are preserved. Only the optional first
space after the "=" or "+=" is not part of the value.

Here is a simple example::

    # Global settings are copied to each section.
    full_text = Hello, World!
    color = #0F0

    [greeting1]

    # Sections can override global variables.
    [greeting2]
    color = #F00

    [greeting3]
    full_text  = Hello,
    full_text +=  Universe!

    [current time]
    command = while true; do date +'%H:%M:%S'; sleep 1; done
    shell = True

This displays "Hello, World!" in green (greeting1) and red (greeting2), then
"Hello, Universe!" in green (greeting3), and finally the time (current time),
which is updated every second.


VARIABLES
=========

Boolean variables accept the values true/false, yes/no, on/off and 1/0.  Case
is ignored.

GLOBAL VARIABLES
----------------

Global variables must be set above all sections and are copied to each section
that doesn't set them locally.

**delay** = *seconds*  (default: 0.1)
    When a worker prints something, i3bf waits for a moment and then checks
    again if any workers have produced additional updates in the mean time.
    This is done to reduce the number of redundant updates for i3bar.  With
    the default value of 0.1, this limits the update rate to 10 updates per
    second regardless of how much output workers produce.

**logfile** = *file*  (default: <not set>)
    Path to file where debugging information is logged.  If no logfile is
    specified, STDERR is used.  During normal operation and with no logfile
    specified debugging output should end up on the STDERR of i3 (which
    usually goes to ~/.xsession-errors).

**show_updates** = *bool*  (default: false)
    This adds a block that displays how many updates per second i3bar had to
    handle during the last ten seconds.


LOCAL VARIABLES
---------------

Local variables can be set locally per section or globally for all sections.
Local settings override global settings.

**command** = *commandline*  (default: <not set>)
    An executable with optional arguments.  See **WORKERS** for more
    information.

**shell** = *boolean*  (default: false)
    Whether to run **command** via **/bin/sh -c** or not.  Note that
    shell-like argument quoting works regardless in any case.

**trigger_updates** = *boolean*  (default: true)
    Whether i3bar is updated when this section's worker pushes an update.  If
    this is disabled, any updates are cached until a worker with
    **trigger_updates** enabled prints something.

**clicks_enabled** = *boolean*  (default: false)
    Whether this worker expects mouse click events on its STDIN or not.  Click
    events are JSON encoded strings as documented at
    https://i3wm.org/docs/i3bar-protocol.html#_click_events.


DYNAMIC COLORS
^^^^^^^^^^^^^^

A dynamic color consists of two or more colors that describe a gradient, and a
numeric value within minimum/maximum limits.  The value is converted to a
percentage or ratio which is used to pick a color on the gradient.

For example, given colors=#FF0000-#00FF00-#0000FF, min=0 and max=100, a value
of 0 would pick #FF0000, 50 would pick #00FF00 and 100 #0000FF.

**dyncolor_colors** = *colorlist*
    A JSON array/list of RGB colors that is used to describe a gradient.
    Valid color notations are "#RRGGBB" and "#RGB".  There is no maximum
    number of colors.

**dyncolor_value** = *number*
    The number used to pick a color from the gradient specified by
    **dyncolor_colors**.

**dyncolor_min** = *number*, **dyncolor_max** = *number*
    The reference points to convert **dyncolor_value** into a percentage.
    This sets hard limits; for example, with **dyncolor_max** set to 100, any
    **dyncolor_value** >= 100 generates the same color.

**dyncolor_softmin** = *number*, **dyncolor_softmax** = *number*
    Instead of hard limits for **dyncolor_value**, these variables set soft or
    temporary limits that will increase/decrease whenever **dyncolor_value**
    is larger/smaller than the current limits.  For example, with
    **dyncolor_max** set to 100, setting **dyncolor_value** to 200 implies
    that **dyncolor_max** is increased to 200.  This is the default with both
    limits set to 0.

**dyncolor_scale** = *scale*  (default: linear)
    To put more emphasis on smaller values, set this to "sqrt" or "log".


I3BAR PROTOCOL VARIABLES
------------------------

All i3bar protocol block variables can be set globally or locally.  They are
documented at http://i3wm.org/docs/i3bar-protocol.html#_blocks_in_detail.

The following variables have extended functionality.

**full_text**, **short_text**
    These variables can contain inline function calls.  The return value of
    the function call replaces the function call in the text.  For example, if
    a worker prints::

        Usage: vbar(50, min=0, max=100)

    i3bf converts the call to vbar(...) to a half-full vertical bar::

        Usage: ▄

    See the **FUNCTIONS** section for more information.

**color**
    Aside from the "#RRGGBB" format, the shorter "#RGB" format is also valid.
    It's also possible to specify dynamic colors like this::

        color = colors=#123-#456:max=100:scale=sqrt:value=75

    Any of the **dyncolor_\*** variables can be set in this format, order does
    not matter, and if a key is not specified it is assumed to be
    **dyncolor_colors** or, if that doesn't make sense, **dyncolor_value**.
    This results in exactly the same color::

        color = 75:max=100:scale=sqrt:#123-#456

    Everything is cached, so this also results in the same color::

        color = scale=sqrt:max=100
        color = #123-#456
        color = 75


WORKER ENVIRONMENT VARIABLES
----------------------------

Any variables not mentioned above are provided to workers as environment
variables.  They can be set globally for all sections or locally for a single
section.

This is useful for worker-specific configuration, but users can set arbitrary
variables in their configuration file.  For example, setting the LANG variable
influences the output of 'date +%x'.


WORKERS
=======

A worker is simply an executable that prints text in an endless loop.  It is
called once when the variable **command** is set, and then read from until it
exits.  Each line a worker prints to STDOUT is displayed on i3bar.  Lines
printed to STDERR are debugging messages and thrown away unless debugging is
enabled by calling i3bf with the **--debug** or **-d** argument.

A line can also be a block (a JSON object/hash/dictionary) or a list/array of
blocks.  Each block can set any of the local variables described above in the
**CONFIGURATION FILE** section.  The main block (i.e. a single block that is
not part of a list) persistently overrides any settings made in the
configuration file.  That means workers can print only changed values.

The main block is used as the basis for any blocks that are part of a list.
For example, if your worker needs pango markup in multiple blocks, you can
enable it forever by printing::

    {"markup": "pango"}

And then use pango markup as you wish::

    [
      {"name": "block1", "full_text": "<small>tiny text</small>"},
      {"name": "block2", "full_text": "<small>more tiny text</small>"},
      ...
    ]

.. note::
   The separator between updates is a newline ("\\n"), meaning that the block
   or list of blocks must be in a single line.  It is shown in multiple lines
   here for readability.

In the example above, each list block has a unique "name" value that is used
as an identifier.  This is necessary because the values of list blocks are
cached, and because blocks can be added and removed at any position in the
list.  A block's ID is unique as long as the combination of "name" and
"instance" are unique, while either one (but not both) can be omitted.


FUNCTIONS
=========

The variables **full_text** and **short_text** are preprocessed to replace
inline function calls with their return values.  The available functions are
documented below.

Arguments can be provided as positional arguments or with keywords. Mixing
positional and keyword arguments is also possible. These function calls are
identical::

    vbar(10, 5, 30)
    vbar(value=10, min=5, max=30)
    vbar(10, min=5, max=30)
    vbar(min=5, 10, max=30)

All function arguments are cached and don't have to be provided again after
the first call.  This makes it possible to initialize, for example, a vertical
bar like this::

    Usage: vbar(min=0, max=100)

After that the height of the bar can be changed like this::

    Usage: vbar(7)
    Usage: vbar(50)
    Usage: vbar(94)

If the same function is called multiple times in the same block, each call
must have a unique ID.  Any string without whitespace is a valid ID.  An ID is
specified by appending it to the name of the function.  This example defines
the vbar instances "_A" and "_B"::

    Usage A: vbar_A(min=0, max=100) - Usage B: vbar_B(min=-500, max=500)
    Usage A: vbar_A(3) - Usage B: vbar_B(-26)
    Usage A: vbar_A(79) - Usage B: vbar_B(288)


AVAILABLE FUNCTIONS
-------------------

**vbar**\(*value*, *min*, *max*, *softmin*, *softmax*, *scale*)
    Display a vertical bar to indicate, for example, hard disk usage or
    network throughput.

    *value*
        A number between *min* and *max* that is converted to a ratio
        ((*value*-*min*)/(*max*-*min*)) which specifies the height of the
        bar.

    *min*, *max*
        The reference points and hard limits for value.

    *softmin*, *softmax*
        Same as *min*\/*max*, but values automatically decrease/increase as
        smaller/larger *value*\s are encountered.

    *scale*
        "linear", "sqrt" or "log". "sqrt" and "log" put more emphasis on
        smaller values.

**hbar**\(*value*, *width*, *min*, *max*, *softmin*, *softmax*, *scale*)
    Display a horizontal bar.  *width* specifies the width of the bar in
    characters.  The minimum width is 1.  See **vbar** for the other
    arguments.


SEE ALSO
========

i3bar(1) i3(1)
