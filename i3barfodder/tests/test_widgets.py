import unittest
from i3barfodder import widgets
from i3barfodder import error


class TestVerticalBar(unittest.TestCase):
    def test_characters(self):
        vb = widgets.VerticalBar(('max',80),)
        self.assertEqual(str(vb), ' ')
        for value,char in ((0,' '), (10,'\u2581'), (20,'\u2582'), (30,'\u2583'),
                           (40,'\u2584'), (50,'\u2585'), (60,'\u2586'), (70,'\u2587'),
                           (80,'\u2588')):
            self.assertEqual(vb.update(('value',value)), char)
            self.assertEqual(str(vb), char)

    def test_initial_value(self):
        vb = widgets.VerticalBar(('max',80), ('value',40))
        self.assertEqual(str(vb), '\u2584')

    def test_copy(self):
        vb = widgets.VerticalBar(('min',-100), ('softmax',-20))
        self.assertEqual(str(vb), ' ')
        vbc = vb.copy()
        for value,char in ((-100,' '), (-90,'\u2581'), (-80,'\u2582'), (-70,'\u2583'),
                           (-60,'\u2584'), (-50,'\u2585'), (-40,'\u2586'), (-30,'\u2587'),
                           (-20,'\u2588')):
            self.assertEqual(vbc.update(('value',value)), char)
            self.assertEqual(str(vbc), char)
        self.assertEqual(str(vb), ' ')


class TestHorizontalBar(unittest.TestCase):
    def test_full_blocks(self):
        tests = {
            # keys are (max,width)
            (60,10): (( 0, '          '),
                      ( 6, '█         '),
                      (12, '██        '),
                      (18, '███       '),
                      (24, '████      '),
                      (30, '█████     '),
                      (36, '██████    '),
                      (42, '███████   '),
                      (48, '████████  '),
                      (54, '█████████ '),
                      (60, '██████████')),
            (10,20): (( 0, '                    '),
                      ( 1, '██                  '),
                      ( 2, '████                '),
                      ( 3, '██████              '),
                      ( 4, '████████            '),
                      ( 5, '██████████          '),
                      ( 6, '████████████        '),
                      ( 7, '██████████████      '),
                      ( 8, '████████████████    '),
                      ( 9, '██████████████████  '),
                      (10, '████████████████████')),
            (2,4): ((0.0, '    '),
                    (0.5, '█   '),
                    (1.0, '██  '),
                    (1.5, '███ '),
                    (2.0, '████')),
        }

        for (maxval,width),cases in tests.items():
            hb = widgets.HorizontalBar(('width',width), ('max',maxval))
            self.assertEqual(str(hb), ' '*width)
            for value,exp in cases:
                barstr = hb.update(('value',value))
                self.assertEqual(barstr, exp)
                self.assertEqual(len(barstr), width)

                self.assertEqual(str(hb), exp)
                self.assertEqual(len(str(hb)), width)

    def test_single_char(self):
        hb = widgets.HorizontalBar(('max',8))
        for value,bar_str in ((0, ' '),
                              (1, '▏'),
                              (2, '▎'),
                              (3, '▍'),
                              (4, '▌'),
                              (5, '▋'),
                              (6, '▊'),
                              (7, '▉'),
                              (8, '█')):
            self.assertEqual(hb.update(('value',value)), bar_str)
            self.assertEqual(str(hb), bar_str)

    def test_multiple_chars(self):
        tests = {
            # keys are (max,width)
            (72,3): ((    0, '   '),
                     (    3, '▏  '),
                     (    6, '▎  '),
                     (    9, '▍  '),
                     (   12, '▌  '),
                     (   15, '▋  '),
                     (   18, '▊  '),
                     (   21, '▉  '),
                     (   24, '█  '),
                     (   27, '█▏ '),
                     (   30, '█▎ '),
                     (   33, '█▍ '),
                     (   36, '█▌ '),
                     (   39, '█▋ '),
                     (   42, '█▊ '),
                     (   45, '█▉ '),
                     (   48, '██ '),
                     (   51, '██▏'),
                     (   54, '██▎'),
                     (   57, '██▍'),
                     (   60, '██▌'),
                     (   63, '██▋'),
                     (   66, '██▊'),
                     (   69, '██▉'),
                     (   72, '███')),
        }
        for (maxval,width),cases in tests.items():
            hb = widgets.HorizontalBar(('width',width), ('max',maxval))
            self.assertEqual(str(hb), ' '*width)
            for value,exp in cases:
                barstr = hb.update(('value',value))
                self.assertEqual(barstr, exp)
                self.assertEqual(str(hb), exp)
