import unittest
from i3barfodder import worker

from json import (loads, dumps)
import io
import os
import random

class TestWorkerAndReadpool(unittest.TestCase):
    readpool = worker.Readpool()

    def assertEqualDict(self, jsonstr, dct):
        self.assertEqual(loads(jsonstr), dct)

    def test_defaults(self):
        w = worker.Worker('test_id', self.readpool)
        self.assertEqualDict(w.json, {'full_text': ''})
        self.assertEqual(w.stdout_pending, False)
        self.assertEqual(w.triggers_updates, True)
        self.assertEqual(w.is_static, True)

    def test_command(self):
        w = worker.Worker('test_id', self.readpool, command='/bin/echo "Rhubarb"')
        w.run()
        self.assertEqual(len(self.readpool._fds), 2)
        self.readpool.read(timeout=0.5)
        self.assertTrue(w.stdout_pending)
        self.assertEqualDict(w.json, {'full_text': 'Rhubarb'})
        self.assertFalse(w.stdout_pending)

        # Auto-remove dead fds from readpool
        self.readpool.read(timeout=0.5)
        self.assertEqual(len(self.readpool._fds), 0)

    def test_command_changing(self):
        w = worker.Worker('test_id', self.readpool, command='/bin/echo "Parsley"')
        w.run()
        self.assertEqual(len(self.readpool._fds), 2)
        self.readpool.read(timeout=0.5)
        self.assertEqualDict(w.json, {'full_text': 'Parsley'})

        # Auto-remove dead fds from readpool
        self.readpool.read(timeout=0.5)
        self.assertEqual(len(self.readpool._fds), 0)

        w._parse_command_stdout(dumps(
            # Command counts to three
            { 'command': 'for i in $(seq 3); do echo "$i"; sleep 0.01; done',
              'shell': True }
        ))
        for i in range(1, 4):
            self.readpool.read(timeout=0.5)
            self.assertTrue(w.stdout_pending)
            self.assertEqualDict(w.json, {'full_text': str(i)})
            self.assertFalse(w.stdout_pending)

        # Auto-remove dead fds from readpool
        self.readpool.read(timeout=0.5)
        self.assertEqual(len(self.readpool._fds), 0)

    def test_too_much_output_from_command(self):
        command = 'while true; do echo "new line"; sleep {delay}; done'
        import logging

        def test_with_delay(delay, should_terminate):
            import time
            w = worker.Worker('test_id', self.readpool, command=command.format(delay=delay),
                              shell=True)
            w.run()
            for _ in range(20):
                self.readpool.read(timeout=0.11)

            if should_terminate:
                self.assertEqual(w._proc, None)
                self.assertRegex(w.json, r'exceed.*lines per second limit')
            else:
                self.assertNotEqual(w._proc, None)
                self.assertEqual(w.json, dumps({'full_text': 'new line'}))

            # Remove worker
            w.terminate()
            self.readpool.read(timeout=0.11)
            self.assertEqual(len(self.readpool._fds), 0)

        test_with_delay(0.1, should_terminate=False)
        test_with_delay(0.05, should_terminate=True)

    def test_multiple_blocks(self):
        w = worker.Worker('test_id', self.readpool)

        # Set defaults for all blocks
        defaults = { 'align': 'left',
                     'min_width': random.randint(100, 300),
                     'markup': random.choice(('none', 'pango')) }
        w._parse_command_stdout(dumps(defaults))
        expectation = defaults.copy()
        expectation.update(full_text='')
        self.assertEqualDict(w.json, expectation)

        # Create a bunch of blocks
        blocks = []
        for i in range(3):
            blocks.append(
                { 'name': 'block{}'.format(i),
                  'full_text': 'This is block #{}'.format(i),
                  'color': '#'+''.join('{:02X}'.format(round(255/2*i)) * 3) }
            )
        blocks[0]['align'] = 'center'

        def compare():
            # Read and parse worker's json with expectation
            w._parse_command_stdout(dumps(blocks))
            result_blocks = loads('['+w.json+']')
            for index,block in enumerate(blocks):
                exp = expectation.copy()
                exp.update(block)
                exp['name'] = 'test_id\u311F' + exp['name']
                self.assertEqual(result_blocks[index], exp)

        compare()

        blocks.append( { 'name': 'blockA',
                         'full_text': 'This is block #A',
                         'urgent': True } )
        compare()
        blocks.insert(0, { 'name': 'blockB',
                           'full_text': 'This is block #B',
                           'urgent': True,
                           'min_width': 100 } )
        blocks[-1]['urgent'] = False
        compare()

        # This should make changes to all blocks
        defaults['min_width'] = 500
        defaults['align'] = 'right'
        w._parse_command_stdout(dumps(defaults))
        expectation.update(defaults)
        compare()

        def swap(i, j):
            temp = blocks[i]
            blocks[i] = blocks[j]
            blocks[j] = temp

        swap(4, 0)
        compare()
        swap(2, 3)
        swap(4, 1)
        swap(1, 2)
        compare()

        del(blocks[-1])
        compare()
        del(blocks[0])
        compare()
        del(blocks[1])
        compare()

    def test_dynamic_colors_in_multiple_blocks(self):
        w = worker.Worker('test_id', self.readpool)

        # Set defaults for all blocks
        defaults = { 'dyncolor_colors': ['#0f0','#f0f'],
                     'dyncolor_min': 0,
                     'dyncolor_softmax': 100 }
        w._parse_command_stdout(dumps(defaults))
        expectation = defaults.copy()
        expectation.update()
        self.assertEqualDict(w.json, { 'full_text': '', 'color': '#00FF00' })

        # Create a bunch of blocks
        blocks = []
        for i in range(5):
            blocks.append(
                { 'name': 'block{}'.format(i),
                  'full_text': 'This is block #{}'.format(i),
                  'dyncolor_value': 100/4*i }
            )
        expected_colors = ['#00FF00', '#40C040', '#808080', '#C040C0', '#FF00FF']

        def compare():
            w._parse_command_stdout(dumps(blocks))
            result_blocks = loads('['+w.json+']')
            for index,block in enumerate(blocks):
                exp = expectation.copy()
                exp.update(block)
                for k in tuple(exp):
                    if k.startswith('dyncolor_'):
                        del(exp[k])
                exp['name'] = 'test_id\u311F' + exp['name']
                exp['color'] = expected_colors[index]
                self.assertEqual(result_blocks[index], exp)

        compare()

        del(blocks[2]['dyncolor_value'])
        blocks[2]['color'] = '#fff'
        expected_colors[2] = '#FFFFFF'
        compare()

        del(blocks[0]['dyncolor_value'])
        blocks[0]['color'] = '#000000'
        expected_colors[0] = '#000000'
        compare()

        del(blocks[2]['color'])
        for value,color in ((0,'#00FF00'), (25,'#40C040'), (50,'#808080'),
                            (75,'#C040C0'), (100,'#FF00FF')):
            blocks[2]['dyncolor_value'] = value
            expected_colors[2] = color
            compare()

        blocks[2]['color'] = '#123456'
        blocks[2]['dyncolor_value'] = 33
        with self.assertLogs(level='ERROR'):
            w._parse_command_stdout(dumps(blocks))

        defaults['color'] = '#1234'
        with self.assertLogs(level='ERROR'):
            w._parse_command_stdout(dumps(defaults))

    def test_last_separator_in_multiple_blocks(self):
        w = worker.Worker('test_id', self.readpool)
        defaults = {}
        expectation = { 'full_text': '' }

        blocks = []
        for i in range(5):
            blocks.append(
                { 'name': 'block{}'.format(i),
                  'separator': False,
                  'separator_block_width': 20 }
            )

        def compare():
            w._parse_command_stdout(dumps(blocks))
            result_blocks = loads('['+w.json+']')
            for index,block in enumerate(blocks):
                exp = expectation.copy()
                exp.update(block)
                exp['name'] = 'test_id\u311F' + exp['name']
                if index == len(blocks)-1:
                    # Last block gets its separator overridden
                    for field in ('separator', 'separator_block_width'):
                        if field in defaults:
                            exp[field] = defaults[field]
                        else:
                            del(exp[field])
                self.assertEqual(result_blocks[index], exp)

        compare()

        defaults['separator'] = True
        w._parse_command_stdout(dumps(defaults))
        compare()
        defaults['separator_block_width'] = 99
        w._parse_command_stdout(dumps(defaults))
        compare()
