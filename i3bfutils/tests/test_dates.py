from i3bfutils import dates
from datetime import (date, timedelta)

THIS_YEAR = date.today().year

def test_date():
    assert dates.str2date('{}-10-19'.format(THIS_YEAR)) == date(THIS_YEAR, 10, 19)

def test_date_without_year():
    assert dates.str2date('10-19') == date(THIS_YEAR, 10, 19)
    assert dates.str2date('10-19', year=3000) == date(3000, 10, 19)

def test_date_names():
    assert dates.str2date('Christmas') == date(THIS_YEAR, 12, 25)
    assert dates.str2date("New Year's Day", year=3000) == date(3000, 1, 1)

def test_easter():
    assert dates.str2date('Easter', year=2014) == date(2014, 4, 20)
    assert dates.str2date('Easter', year=2015) == date(2015, 4, 5)
    assert dates.str2date('Easter', year=2016) == date(2016, 3, 27)
    assert dates.str2date('Easter', year=2017) == date(2017, 4, 16)
    assert dates.str2date('Easter', year=2018) == date(2018, 4, 1)

def test_delta():
    for datestr,exp in (('03-30+2d', date(THIS_YEAR, 3, 30) + timedelta(days=2)),
                        ('01-30-60d', date(THIS_YEAR, 1, 30) - timedelta(days=60)),
                        ('06-10-1w', date(THIS_YEAR, 6, 10) - timedelta(days=7)),
                        ('10-16+300w', date(THIS_YEAR, 10, 16) + timedelta(days=300*7)),
                        ('Christmas+10w', date(THIS_YEAR, 12, 25) + timedelta(days=10*7))):
        test_date = dates.str2date(datestr)
        assert exp == test_date

def test_multiple_deltas():
    for year in (2015, 2016, 2017, 2018, 2019, 2020):
        d1 = dates.str2date('Good Friday+3d-4d-5d+6d', year)
        d2 = dates.str2date('Easter+2w-16d', year)
        assert d1 == d2

def test_weekday_delta():
    # Advent Sundays
    for advent,exp in (('2015-12-25-1Sun', date(2015, 12, 20)),
                       ('2015-12-25-2Sun', date(2015, 12, 13)),
                       ('2015-12-25-3Sun', date(2015, 12, 6)),
                       ('2015-12-25-4Sun', date(2015, 11, 29))):
        assert dates.str2date(advent) == exp

    for advent,exp in (('2016-12-25-1sun', date(2016, 12, 18)),
                       ('2016-12-25-2sun', date(2016, 12, 11)),
                       ('2016-12-25-3sun', date(2016, 12, 4)),
                       ('2016-12-25-4sun', date(2016, 11, 27))):
        assert dates.str2date(advent) == exp

    for advent,exp in (('2017-12-25-1sun', date(2017, 12, 24)),
                       ('2017-12-25-2sun', date(2017, 12, 17)),
                       ('2017-12-25-3sun', date(2017, 12, 10)),
                       ('2017-12-25-4sun', date(2017, 12, 3))):
        assert dates.str2date(advent) == exp

    for advent,exp in (('2018-12-25-1sun', date(2018, 12, 23)),
                       ('2018-12-25-2sun', date(2018, 12, 16)),
                       ('2018-12-25-3sun', date(2018, 12, 9)),
                       ('2018-12-25-4sun', date(2018, 12, 2))):
        assert dates.str2date(advent) == exp

    for advent,exp in (('2019-12-25-1sun', date(2019, 12, 22)),
                       ('2019-12-25-2sun', date(2019, 12, 15)),
                       ('2019-12-25-3sun', date(2019, 12, 8)),
                       ('2019-12-25-4sun', date(2019, 12, 1))):
        assert dates.str2date(advent) == exp

    # 2016-12-25 is a Sunday; '-1sun' should move one week back (as tested
    # above), but '-0sun' or '+0sun' should return the same date.
    assert dates.str2date('2016-12-25-0sun') == date(2016, 12, 25)
    assert dates.str2date('2016-12-25+0sun') == date(2016, 12, 25)

    # German "Buß- und Bettag":
    # https://en.wikipedia.org/wiki/Bu%C3%9F-_und_Bettag
    for dstr,exp in (('2015-12-25-5sun-1wed', date(2015, 11, 18)),
                     ('2016-12-25-5sun-1wed', date(2016, 11, 16)),
                     ('2017-12-25-5sun-1wed', date(2017, 11, 22)),
                     ('2018-12-25-5sun-1wed', date(2018, 11, 21)),
                     ('2019-12-25-5sun-1wed', date(2019, 11, 20))):
        assert dates.str2date(dstr) == exp

    for dstr,exp in (('2015-12-25-4sun-11d', date(2015, 11, 18)),
                     ('2016-12-25-4sun-1w-4d', date(2016, 11, 16)),
                     ('2017-12-25-4sun-11d', date(2017, 11, 22)),
                     ('2018-12-25-4sun-1w-4d', date(2018, 11, 21)),
                     ('2019-12-25-4sun-11d', date(2019, 11, 20))):
        assert dates.str2date(dstr) == exp

    for dstr,exp in (('2015-11-23-1wed', date(2015, 11, 18)),
                     ('2016-11-23-1wed', date(2016, 11, 16)),
                     ('2017-11-23-1wed', date(2017, 11, 22)),
                     ('2018-11-23-1wed', date(2018, 11, 21)),
                     ('2019-11-23-1wed', date(2019, 11, 20)),
                     ('2020-11-23-1wed', date(2020, 11, 18)),
                     ('2021-11-23-1wed', date(2021, 11, 17)),
                     ('2022-11-23-1wed', date(2022, 11, 16))):
        assert dates.str2date(dstr) == exp

    # More random dates with future weekdays
    for dstr,exp in (('2015-12-25+5thu', date(2015, 12, 31) + timedelta(days=4*7)),
                     ('2015-12-24+5thu', date(2015, 12, 31) + timedelta(days=4*7)),
                     ('2015-12-23+5thu', date(2015, 12, 24) + timedelta(days=4*7)),
                     ('2015-12-24+0thu', date(2015, 12, 24)),
                     ('2015-12-24+0thu+7w', date(2015, 12, 24) + timedelta(days=7*7)),
                     ('2015-12-24+0thu+1w-2mo', date(2015, 12, 21)),
                     ('2015-12-24+1thu+1w-2mo', date(2015, 12, 28))):
        assert dates.str2date(dstr, exp.year) == exp


def test_alarm_during_year_transition():
    alarms = dates.AlarmIndicator(dates=(date(THIS_YEAR+1, 1, 1),),
                                  alarmindicators=('...', '..', '.'))

    import time
    orig_time = time.time

    for dt,indstr in ((date(THIS_YEAR, 12, 29), ''),
                      (date(THIS_YEAR, 12, 30), '.'),
                      (date(THIS_YEAR, 12, 31), '..'),
                      (date(THIS_YEAR+1, 1, 1), '...')):
        time.time = lambda: int(dt.strftime('%s'))
        assert alarms.indicator == indstr

    time.time = orig_time
