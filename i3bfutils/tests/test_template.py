from i3bfutils import template
import unittest

class TestTemplate(unittest.TestCase):
    def test_normal_string(self):
        t = template.Template('hello', 'test')
        blocks = t.make_blocks({}, init=True)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'hello',
                                   'separator': False, 'separator_block_width': 0}])
        blocks = t.make_blocks({}, init=False)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'hello'}])

    def test_layout_with_separator(self):
        t = template.Template('[hello]4', 'test')
        blocks = t.make_blocks({}, init=True)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'hello',
                                   'separator': False, 'separator_block_width': 4}])
        blocks = t.make_blocks({}, init=False)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'hello'}])

        t = template.Template('[hello]10|', 'test')
        blocks = t.make_blocks({}, init=True)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'hello',
                                   'separator': True, 'separator_block_width': 10}])
        blocks = t.make_blocks({}, init=False)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'hello'}])

        t = template.Template('[hello]|', 'test')
        blocks = t.make_blocks({}, init=True)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'hello',
                                   'separator': True, 'separator_block_width': 0}])
        blocks = t.make_blocks({}, init=False)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'hello'}])

    def test_multiple_blocks(self):
        t = template.Template('[one][two] [four]10[five]|', 'test')
        blocks = t.make_blocks({}, init=True)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'one',
                                   'separator': False, 'separator_block_width': 0},
                                  {'name': 'test', 'instance': '2', 'full_text': 'two',
                                   'separator': False, 'separator_block_width': 0},
                                  {'name': 'test', 'instance': '3', 'full_text': ' ',
                                   'separator': False, 'separator_block_width': 0},
                                  {'name': 'test', 'instance': '4', 'full_text': 'four',
                                   'separator': False, 'separator_block_width': 10},
                                  {'name': 'test', 'instance': '5', 'full_text': 'five',
                                   'separator': True, 'separator_block_width': 0}])
        blocks = t.make_blocks({}, init=False)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'one'},
                                  {'name': 'test', 'instance': '2', 'full_text': 'two'},
                                  {'name': 'test', 'instance': '3', 'full_text': ' '},
                                  {'name': 'test', 'instance': '4', 'full_text': 'four'},
                                  {'name': 'test', 'instance': '5', 'full_text': 'five'}])

    def test_single_color(self):
        t = template.Template('[red|#F00]', 'test')
        blocks = t.make_blocks({}, init=True)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'red',
                                   'color': 'colors=#F00',
                                   'separator': False, 'separator_block_width': 0}])
        blocks = t.make_blocks({}, init=False)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'red'}])

    def test_dynamic_color(self):
        # both layouts should be identical
        for layout in ('[red|value={x}:colors=#F00-#00F]', '[red|#F00-#00F:{x}]'):
            t = template.Template(layout, 'test')
            blocks = t.make_blocks({'x': 12}, init=True)
            self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'red',
                                       'color': 'value=12:colors=#F00-#00F',
                                       'separator': False, 'separator_block_width': 0}])
            blocks = t.make_blocks({'x': 782}, init=False)
            self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'red',
                                       'dyncolor_value': '782'}])

    def test_dynamic_color_with_default_limits(self):
        layout = ('[x={x} text|{x}:#F00-#00F]'
                  '[y={y} text|{y}:#0F0-#0F0]'
                  '[z={z} text|{z}:#00F-#F00]')
        dflt_limits = {'max': {'{x}': 15, '{y}': 0},
                       'min': {'{x}': 12},
                       'softmin': {'{y}': -1000}}
        t = template.Template(layout, 'test', dflt_limits)
        blocks = t.make_blocks({'x': 12, 'y': -300, 'z': 4}, init=True)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'x=12 text',
                                  'color': 'max=15:min=12:value=12:colors=#F00-#00F',
                                  'separator': False, 'separator_block_width': 0},
                                 {'name': 'test', 'instance': '2', 'full_text': 'y=-300 text',
                                  'color': 'max=0:softmin=-1000:value=-300:colors=#0F0-#0F0',
                                  'separator': False, 'separator_block_width': 0},
                                 {'name': 'test', 'instance': '3', 'full_text': 'z=4 text',
                                  'color': 'value=4:colors=#00F-#F00',
                                  'separator': False, 'separator_block_width': 0}])
        blocks = t.make_blocks({'x': 13, 'y': 0, 'z': 700}, init=False)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1',
                                   'full_text': 'x=13 text', 'dyncolor_value': '13'},
                                  {'name': 'test', 'instance': '2',
                                   'full_text': 'y=0 text', 'dyncolor_value': '0'},
                                  {'name': 'test', 'instance': '3',
                                   'full_text': 'z=700 text', 'dyncolor_value': '700'}])

    def test_dynamic_color_with_default_limits_and_user_limits(self):
        dflt_limits = {'max': {'{x}': 15}, 'min': {'{x}':12}}
        t = template.Template('[text|{x}:#F00-#00F:min=0:softmax=100]', 'test', dflt_limits)
        blocks = t.make_blocks({'x': 12}, init=True)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'text',
                                   'color': 'max=15:min=12:value=12:colors=#F00-#00F:min=0:softmax=100',
                                   'separator': False, 'separator_block_width': 0}])
        blocks = t.make_blocks({'x': 1000}, init=False)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1', 'full_text': 'text',
                                   'dyncolor_value': '1000'}])

    def test_custom_fillin_function(self):
        t = template.Template('[text|#F00-#00F:value=xxx:min=0]', 'test')

        def count_xs(string):
            import re
            return re.sub('(x+)', lambda match: '[x*{}]'.format(match.group(0).count('x')), string)

        blocks = t.make_blocks(count_xs, init=False)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1',
                                   'full_text': 'te[x*1]t', 'dyncolor_value': '[x*3]'}])

        def uppercase_xs(string):
            return string.replace('x', 'X')

        blocks = t.make_blocks(uppercase_xs, init=False)
        self.assertEqual(blocks, [{'name': 'test', 'instance': '1',
                                   'full_text': 'teXt', 'dyncolor_value': 'XXX'}])


class TestPrettyDict(unittest.TestCase):
    def test_default_prettifier(self):
        pd = template.PrettyDict()
        pd['12'] = 12
        self.assertEqual(pd['12'], '12')
        self.assertEqual(pd['_12'], 12)
        pd[12] = 12
        self.assertEqual(pd[12], 12)

    def test_custom_prettifiers(self):
        pd = template.PrettyDict(
            prettifiers={ 'percent': lambda p: '{:.0f} %'.format(p),
                          'bytes': lambda b: '{:.1f} MB'.format(b/1e6),
                          'password': lambda pw: '*'*len(pw) },
            percent=29.12345,
            bytes=27.9123e6,
            password='123',
        )
        self.assertEqual(pd['percent'], '29 %')
        self.assertEqual(pd['_percent'], 29.12345)
        pd['percent'] = 17
        self.assertEqual(pd['percent'], '17 %')
        self.assertEqual(pd['_percent'], 17)

        self.assertEqual(pd['bytes'], '27.9 MB')
        self.assertEqual(pd['_bytes'], 27.9123e6)
        pd['bytes'] = 4e9
        self.assertEqual(pd['bytes'], '4000.0 MB')
        self.assertEqual(pd['_bytes'], 4000000000)

        self.assertEqual(pd['password'], '***')
        self.assertEqual(pd['_password'], '123')
        pd['password'] = '456'
        self.assertEqual(pd['password'], '***')
        self.assertEqual(pd['_password'], '456')

        pd['numbers'] = [1,2,3,4,5]
        self.assertEqual(pd['_numbers'], [1,2,3,4,5])
        self.assertEqual(pd['numbers'], '[1, 2, 3, 4, 5]')

    def test_callable_values(self):
        counter = 0
        def get_counter():
            nonlocal counter
            counter += 1
            return counter

        pd = template.PrettyDict(
            prettifiers={ 'counter': lambda c: 'counter = '+str(c) },
            counter=get_counter,
        )

        self.assertEqual(pd['counter'], 'counter = 1')
        self.assertEqual(pd['_counter'], 2)
        self.assertEqual(pd['_counter'], 3)
        self.assertEqual(pd['_counter'], 4)
        self.assertEqual(pd['counter'], 'counter = 5')
